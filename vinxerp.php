<?php
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

if (!defined('_PS_VERSION_')) {
    exit;
}

class Vinxerp extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'vinxerp';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'dbuglab';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('vinxerp');
        $this->description = $this->l('Fetch data in PrestaShop');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        Configuration::updateValue('VINXERP_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayBackOfficeHeader') &&
            $this->registerHook('actionAdminControllerSetMedia');
    }

    public function uninstall()
    {
        Configuration::deleteByName('VINXERP_LIVE_MODE');

        return parent::uninstall();
    }

    public function getContent()
    {
        if ((bool)Tools::isSubmit('submitVinxerpModule')) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitVinxerpModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'VINXERP_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'VINXERP_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'VINXERP_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getConfigFormValues()
    {
        return array(
            'VINXERP_LIVE_MODE' => Configuration::get('VINXERP_LIVE_MODE', true),
            'VINXERP_ACCOUNT_EMAIL' => Configuration::get('VINXERP_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'VINXERP_ACCOUNT_PASSWORD' => Configuration::get('VINXERP_ACCOUNT_PASSWORD', null),
        );
    }

    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }

        // Get the access token
        $accessToken = $this->getAccessToken();

        if ($accessToken) {
            // Make the API call after saving the form
            $this->makeApiCall($accessToken);
        } else {
            // Log an error or handle the case where the access token is not available
            $this->logApiError('Access token not available');
        }
    }

    protected function getAccessToken()
    {
        $client = new Client();

        try {
            $response = $client->post('https://vinxapi.i-ag.ch/api/Identity/Realm/RequestToken', [
                'headers' => [
                    'accept' => 'application/json;odata.metadata=minimal;odata.streaming=true',
                    'Content-Type' => 'application/json;odata.metadata=minimal;odata.streaming=true',
                ],
                'json' => [
                    'client_id' => 'Integrated',
                    'scope' => 'VinX.Shop',
                    'username' => 'VinXAPI',
                    'password' => 'keCescArel6',
                ],
            ]);

            $responseData = json_decode($response->getBody(), true);

            return isset($responseData['access_token']) ? $responseData['access_token'] : null;
        } catch (\Exception $e) {
            // Handle the exception (e.g., log the error)
            $this->logApiError($e->getMessage());
            return null;
        }
    }

    protected function makeApiCall($accessToken)
    {
        $liveMode = Configuration::get('VINXERP_LIVE_MODE', true);
        $apiEndpoint = 'https://vinxapi.i-ag.ch/api/v1.0/Core/VinX/Shop/BaseData/Article';
        $apiParams = array();

        $client = new Client();
        try {
            $response = $client->get($apiEndpoint, [
                'headers' => [
                    'accept' => 'application/json;odata.metadata=minimal;odata.streaming=true',
                    'Authorization' => 'Bearer ' . $accessToken,
                ],
            ]);
            $decodedResponse = json_decode($response->getBody(), true);

            //$this->createCategory($decodedResponse, $accessToken);
            // Log the response or perform other actions as needed
            $this->addProductToPrestaShop($decodedResponse, $accessToken);
        } catch (\Exception $e) {
            // API call failed, handle the error
            $this->logApiError($e->getMessage());
        }
    }

    protected function addProductToPrestaShop($decodedResponse, $accessToken)
    {
        $categoriesResponse = $this->getArticleCategories($accessToken);
        $categories = json_decode($categoriesResponse, true);
        $categoryIds = array_column($categories, 'id');
        if (!empty($decodedResponse)) {
            foreach ($decodedResponse as $item) {
                $db = Db::getInstance();
                $sql = "SELECT id_product FROM ps_product_lang WHERE name LIKE '%" . $item['designation'] . "%'";
                $existing_product_ids = $db->ExecuteS($sql);
                if (!empty($existing_product_ids)) {
                    echo 'Product with name "' . $item['designation'] . '" already exists';
                    foreach ($existing_product_ids as $product_id) {
                        echo '<br>Product ID: ' . $product_id['id'];
                    }
                } else {
                    $categoryId = $item['articleCategoryId'];
                    $categoryId = isset($item['articleCategoryId']) ? $item['articleCategoryId'] : null;
                    if (!in_array($categoryId, $categoryIds)) {
                        continue; // Skip products without a valid category ID
                    }
                    // Use array_filter to find the category with the specified ID
                    $filteredCategories = array_filter($categories, function ($category) use ($categoryId) {
                        return $category['id'] == $categoryId;
                    });

                    $categoryId = null;
                    $catid = null;
                    $categoryName = null;
                    // Check if any categories were found
                    if (!empty($filteredCategories)) {
                        // Get the first category (if there are multiple matches, you can adjust as needed)
                        $category = reset($filteredCategories);
                        $categoryName = $category['designation'];
                        $categoryId = $category['id'];
                    }

                    // Assuming $item['articleCategoryId'] contains the category ID
                    $category = new Category($categoryId);
                    // You can then check if the category exists and retrieve its data
                    if (Validate::isLoadedObject($category)) {
                        // Category found
                        $catid = $category->id;
                        $categoryName = $category->name;
                    } else {
                        $object = new Category();
                        $object->name = array(Configuration::get('PS_LANG_DEFAULT') => $categoryName);
                        $object->id_parent = Configuration::get('PS_HOME_CATEGORY');
                        $object->link_rewrite = array(Configuration::get('PS_LANG_DEFAULT') => 'cool-url');
                        if ($object->add()) {
                            $catid = $object->id;
                        }
                    }

                    $product = new Product();
                    $product->name = isset($item['designation']) ? $item['designation'] : 'no value';
                    $product->description = isset($item['searchTerm']) ? $item['searchTerm'] : 'no value';
                    $product->price = isset($item['basePrice']) ? $item['basePrice'] : (isset($item['priceUnit']) ? $item['priceUnit'] : 1.3);
                    $product->quantity = isset($item['quantity']) ? $item['quantity'] : 0;
                    $product->id_category_default = $catid;

                    try {
                        if ($product->add()) {
                            echo 'Product added: ' . $product->name;
                        } else {
                            echo 'Failed to add product: ' . $product->name;
                        }
                    } catch (\Exception $e) {
                        echo 'Product error: ' . $e->getMessage();
                    }
                }
            }
        } else {
            echo 'No products to add.';
        }
    }
    
    protected function getArticleCategories($accessToken)
    {
        $client = new Client();  
        $response = $client->request('GET', 'https://vinxapi.i-ag.ch/api/v1.0/Core/VinX/Shop/BaseData/ArticleCategory', [
            'headers' => [
                'Accept' => 'application/json;odata.metadata=minimal;odata.streaming=true',
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);  
        return $response->getBody()->getContents();
    }
 
}
